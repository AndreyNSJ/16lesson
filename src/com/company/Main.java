package com.company;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Integer[] arrOfInt = {3, 2, 1, 6};
        String[] arrOfStr = {"Table", "Pen", "Box"};
        System.out.println(toList(arrOfInt));
        System.out.println(toList(arrOfStr));
    }

    public static <T> List<T> toList(T[] arr){
        return Arrays.asList(arr);
    }
}
